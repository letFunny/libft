/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strncmp.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: acarrete <acarrete@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/11/13 19:07:57 by acarrete          #+#    #+#             */
/*   Updated: 2019/11/13 19:07:59 by acarrete         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static size_t	ft_min(size_t a, size_t b)
{
	return (a < b ? a : b);
}

int				ft_strncmp(const char *s1, const char *s2, size_t n)
{
	size_t	len;

	len = ft_min(ft_min(n, ft_strlen(s1) + 1), ft_strlen(s2) + 1);
	return (ft_memcmp(s1, s2, len));
}
