/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_countchrs.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: acarrete <acarrete@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/11/13 19:14:55 by acarrete          #+#    #+#             */
/*   Updated: 2019/11/13 19:14:55 by acarrete         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

int	ft_countchrs(char const *s, char const *set)
{
	size_t	i;
	size_t	j;
	int		res;

	i = 0;
	res = 0;
	while (s[i])
	{
		j = 0;
		while (set[j])
		{
			if (s[i] == set[j])
				res++;
			j++;
		}
		i++;
	}
	return (res);
}
