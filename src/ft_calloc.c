/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_calloc.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: acarrete <acarrete@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/11/13 19:15:26 by acarrete          #+#    #+#             */
/*   Updated: 2019/11/13 19:15:37 by acarrete         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	*ft_calloc(size_t nmbemb, size_t size)
{
	void	*ptr;
	size_t	len;

	len = nmbemb * size;
	ptr = malloc(len);
	if (NULL == ptr)
		return (NULL);
	ft_bzero(ptr, len);
	return (ptr);
}
