/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strdupc.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: acarrete <acarrete@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/11/13 19:10:27 by acarrete          #+#    #+#             */
/*   Updated: 2019/11/13 19:10:41 by acarrete         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strdupc(const char *s, int c)
{
	char	*ptr;
	size_t	size;
	char	*pos;

	pos = ft_strchr(s, c);
	if (pos == NULL)
		return (ft_strdupc(s, 0));
	size = pos - s;
	ptr = malloc(sizeof(char) * (size + 1));
	ft_memcpy(ptr, s, size);
	ptr[size] = 0;
	return (ptr);
}
