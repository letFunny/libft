/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strrev.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: acarrete <acarrete@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/11/13 19:04:21 by acarrete          #+#    #+#             */
/*   Updated: 2019/11/13 19:04:28 by acarrete         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strrev(char const *str)
{
	char	*ptr;
	int		str_len;
	int		i;

	str_len = ft_strlen(str);
	ptr = malloc(sizeof(char) * (str_len + 1));
	if (NULL == ptr)
		return (NULL);
	i = 0;
	while (i < str_len)
	{
		ptr[str_len - i - 1] = str[i];
		i++;
	}
	ptr[str_len] = 0;
	return (ptr);
}
