/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strtrim.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: acarrete <acarrete@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/11/13 19:03:26 by acarrete          #+#    #+#             */
/*   Updated: 2019/12/18 15:13:17 by acarrete         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static size_t	count_chrs_beginning(char const *s1, char const *set)
{
	size_t	i;
	size_t	j;
	int		found;

	i = 0;
	while (s1[i])
	{
		j = 0;
		found = 0;
		while (set[j] && !found)
		{
			if (set[j++] == s1[i])
				found = 1;
		}
		if (found == 0)
			return (i);
		i++;
	}
	return (i);
}

static size_t	count_chrs_end(char const *s1, char const *set)
{
	int		i;
	int		j;
	size_t	s1_len;
	size_t	set_len;
	int		found;

	s1_len = ft_strlen(s1);
	set_len = ft_strlen(set);
	i = s1_len - 1;
	while (i >= 0)
	{
		j = set_len - 1;
		found = 0;
		while (j >= 0 && !found)
		{
			if (set[j--] == s1[i])
				found = 1;
		}
		if (found == 0)
			return (s1_len - i - 1);
		i--;
	}
	return (s1_len);
}

char			*ft_strtrim(char const *s1, char const *set)
{
	char	*ptr;
	size_t	beg;
	int		len;

	if (NULL == s1 || NULL == set)
		return (NULL);
	beg = count_chrs_beginning(s1, set);
	len = ft_strlen(s1) - beg - count_chrs_end(s1, set);
	if (len++ <= 0)
		len = 1;
	ptr = malloc(sizeof(char) * len);
	if (NULL == ptr)
		return (NULL);
	ft_strlcpy(ptr, s1 + beg, len);
	return (ptr);
}
