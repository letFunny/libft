/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_atoi.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: acarrete <acarrete@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/11/13 19:16:38 by acarrete          #+#    #+#             */
/*   Updated: 2019/11/13 19:16:39 by acarrete         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

int	ft_atoi(const char *nptr)
{
	size_t	i;
	long	res;
	long	prev_res;
	int		sign;

	i = 0;
	sign = 1;
	while (ft_isspace(nptr[i]))
		i++;
	if (nptr[i] == '+' || nptr[i] == '-')
		sign = (nptr[i++] == '-' ? -1 : 1);
	res = 0;
	while (ft_isdigit(nptr[i]))
	{
		prev_res = res * 10 + (nptr[i++] - '0');
		if (prev_res < res)
			return (sign < 0 ? 0 : -1);
		res = prev_res;
	}
	return (sign * res);
}
