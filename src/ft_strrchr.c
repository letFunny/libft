/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strrchr.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: acarrete <acarrete@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/11/13 19:07:23 by acarrete          #+#    #+#             */
/*   Updated: 2019/11/13 19:07:26 by acarrete         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strrchr(const char *s, int c)
{
	char *o;

	o = NULL;
	while (*s)
	{
		if (*s == c)
			o = (char *)s;
		s++;
	}
	if (c == 0)
		o = (char *)s;
	return (o);
}
