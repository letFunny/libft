/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstmap.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: acarrete <acarrete@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/11/13 19:12:12 by acarrete          #+#    #+#             */
/*   Updated: 2019/11/13 19:30:28 by acarrete         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

t_list	*ft_lstmap(t_list *lst, void *(*f)(void *), void (*del)(void *))
{
	t_list	*res;
	t_list	*next;

	if (NULL == lst || NULL == f || NULL == del)
		return (NULL);
	res = NULL;
	while (NULL != lst)
	{
		if ((next = ft_lstnew(f(lst->content))) == NULL)
		{
			ft_lstclear(&res, del);
			return (NULL);
		}
		ft_lstadd_back(&res, next);
		lst = lst->next;
	}
	return (res);
}
