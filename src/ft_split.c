/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_split.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: acarrete <acarrete@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/11/13 19:07:09 by acarrete          #+#    #+#             */
/*   Updated: 2019/12/15 17:21:46 by acarrete         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static int	ft_countwords(char const *s, char c)
{
	size_t	n_words;
	char	*ptr;

	n_words = 0;
	ptr = (char *)s;
	while (ptr != NULL && *ptr)
	{
		while (*ptr == c)
			ptr++;
		n_words += *ptr == 0 ? 0 : 1;
		ptr = ft_strchr(ptr, c);
	}
	return (n_words);
}

static void	*free_all(char **ptr, int i)
{
	while (i >= 0)
	{
		free(ptr[i]);
		i--;
	}
	free(ptr);
	return (NULL);
}

char		**ft_split(char const *s, char c)
{
	int		n_words;
	int		i;
	char	**words;
	char	*ptr;

	if (NULL == s)
		return (NULL);
	n_words = ft_countwords(s, c);
	words = malloc(sizeof(char *) * (n_words + 1));
	if (words == NULL)
		return (NULL);
	ptr = (char *)s;
	i = 0;
	while (i < n_words)
	{
		while (*ptr == c)
			ptr++;
		words[i] = ft_strdupc(ptr, c);
		if (NULL == words[i])
			return (free_all(words, i - 1));
		ptr = ft_strchr(ptr, c);
		i++;
	}
	words[n_words] = NULL;
	return (words);
}
