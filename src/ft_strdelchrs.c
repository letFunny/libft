/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strdelchrs.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: acarrete <acarrete@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/11/13 19:11:00 by acarrete          #+#    #+#             */
/*   Updated: 2019/11/13 19:11:01 by acarrete         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strdelchrs(char const *s1, char const *set)
{
	char	*ptr;
	size_t	i;

	ptr = malloc(sizeof(char) * (1 + ft_strlen(s1) - ft_countchrs(s1, set)));
	if (NULL == ptr)
		return (NULL);
	i = 0;
	while (*s1)
	{
		if (ft_strchr(set, *s1) == NULL)
			ptr[i++] = *s1;
		s1++;
	}
	ptr[i] = 0;
	return (ptr);
}
