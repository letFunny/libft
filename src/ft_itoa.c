/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_itoa.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: acarrete <acarrete@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/11/13 19:13:05 by acarrete          #+#    #+#             */
/*   Updated: 2019/11/13 19:13:06 by acarrete         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static int	n_digits(long n)
{
	int	i;

	i = 1;
	while (n >= 10)
	{
		n /= 10;
		i++;
	}
	return (i);
}

char		*ft_itoa(int n)
{
	int		sign;
	long	nbr;
	char	*res;
	size_t	i;
	size_t	res_len;

	nbr = n;
	if ((sign = n) < 0)
		nbr = -nbr;
	res_len = n_digits(nbr) + (sign < 0 ? 1 : 0);
	res = malloc(sizeof(char) * (res_len + 1));
	if (NULL == res)
		return (NULL);
	i = 1;
	while (nbr >= 10)
	{
		res[res_len - i] = nbr % 10 + '0';
		nbr /= 10;
		i++;
	}
	res[res_len - i] = nbr % 10 + '0';
	if (sign < 0)
		res[0] = '-';
	res[res_len] = 0;
	return (res);
}
