#ifndef ARRAYLIST_H
# define ARRAYLIST_H

# define ARLST_INITIAL_SIZE 30

# include <stdlib.h>
# include <libft.h>

typedef struct	s_arlst
{
	void		*content;
	size_t		cur_size;
	size_t		max_size;
	size_t		bytes;
}				t_arlst;

void	*ft_realloc(void *ptr, size_t cur_size, size_t new_size);
t_arlst	*ft_arlstnew(size_t bytes);
t_arlst	*ft_arlstconcat(t_arlst *lst, void *ptr, size_t n);

#endif
