#include <arraylist.h>

t_arlst	*ft_arlstconcat(t_arlst *lst, void *ptr, size_t n)
{

	if (n > lst->max_size - lst->cur_size)
	{
		if ((lst = ft_realloc(lst, lst->cur_size, lst->max_size * 2)) == NULL)
			return (NULL);
		lst->max_size *= 2;
	}
	ft_memcpy(lst->content + lst->cur_size * lst->bytes, ptr, n * lst->bytes);
	return (lst);
}
