#include "arraylist.h"

void	*ft_realloc(void *ptr, size_t cur_size, size_t new_size)
{
	void	*n_ptr;

	n_ptr = malloc(new_size);
	if (NULL == n_ptr)
		return (NULL);
	ft_memcpy(n_ptr, ptr, cur_size);
	return (n_ptr);
}
