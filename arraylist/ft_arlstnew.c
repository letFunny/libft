#include "arraylist.h"

t_arlst	*ft_arlstnew(size_t bytes)
{
	t_arlst	*lst;

	lst = malloc(sizeof(t_arlst));
	if (NULL == lst)
		return (NULL);
	lst->content = malloc(ARLST_INITIAL_SIZE * bytes);
	if (NULL == lst->content)
		return (NULL);
	lst->bytes = bytes;
	lst->max_size = ARLST_INITIAL_SIZE;
	lst->cur_size = 0;
	return (lst);
}
