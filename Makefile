# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: acarrete <acarrete@student.42madrid>       +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2019/11/13 19:48:38 by acarrete          #+#    #+#              #
#    Updated: 2019/11/21 19:04:18 by acarrete         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME = libft.a

AR_SRC_DIR = arraylist
SRC_DIR = src
OBJ_DIR = obj
SRC := ft_atoi.c \
ft_bzero.c \
ft_calloc.c \
ft_countchrs.c \
ft_isalnum.c \
ft_isalpha.c \
ft_isascii.c \
ft_isdigit.c \
ft_islower.c \
ft_isprint.c \
ft_isspace.c \
ft_isupper.c \
ft_itoa.c \
ft_memccpy.c \
ft_memchr.c \
ft_memcmp.c \
ft_memcpy.c \
ft_memmove.c \
ft_memset.c \
ft_putchar_fd.c \
ft_putendl_fd.c \
ft_putnbr_fd.c \
ft_putstr_fd.c \
ft_split.c \
ft_strchr.c \
ft_strdelchrs.c \
ft_strdup.c \
ft_strdupc.c \
ft_strjoin.c \
ft_strlcat.c \
ft_strlcpy.c \
ft_strlen.c \
ft_strmapi.c \
ft_strncmp.c \
ft_strnstr.c \
ft_strrchr.c \
ft_strrev.c \
ft_strtrim.c \
ft_substr.c \
ft_tolower.c \
ft_toupper.c \
ft_lstadd_back.c \
ft_lstadd_front.c \
ft_lstclear.c \
ft_lstdelone.c \
ft_lstiter.c \
ft_lstlast.c \
ft_lstmap.c \
ft_lstnew.c \
ft_lstsize.c

AR_SRC := ft_arlstconcat.c \
ft_arlstnew.c \
ft_realloc.c

AR_SRC := $(addprefix $(AR_SRC_DIR)/,$(AR_SRC))
SRC := $(addprefix $(SRC_DIR)/,$(SRC))
AR_OBJ = $(subst $(AR_SRC_DIR), $(OBJ_DIR), $(subst .c,.o,$(AR_SRC)))
OBJ = $(subst $(SRC_DIR), $(OBJ_DIR), $(subst .c,.o,$(SRC)))

CFLAGS = -Wall -Werror -Wextra -Iinc

all: $(NAME)

so: $(OBJ)
	$(CC) -shared -o libft.so $(OBJ)

$(NAME): $(OBJ) $(AR_OBJ)
	ar rc $(NAME) $(OBJ) $(AR_OBJ)
	ranlib $(NAME)

$(OBJ_DIR)/%.o: $(AR_SRC_DIR)/%.c
	$(CC) $(CFLAGS) -c $< -o $@

$(OBJ_DIR)/%.o: $(SRC_DIR)/%.c
	$(CC) $(CFLAGS) -c $< -o $@

clean:
	rm -f $(OBJ)

fclean: clean
	rm -f $(NAME)

re: fclean all

.PHONY: clean fclean all re
